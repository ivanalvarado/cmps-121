package com.cs121.hw2.hw2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.util.Log;
import android.widget.TextView;

import com.cs121.hw2.hw2.pojo.Conditions;
import com.cs121.hw2.hw2.pojo.Example;
import com.cs121.hw2.hw2.pojo.ObservationLocation;
//import com.cs121.hw2.hw2.pojo.Response;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {

    private String user_id;
    public static String LOG_TAG = "MyApplication";
    String result = "";
    String city = "";
    String tempF = "";
    String tempC = "";
    String relHum = "";
    String windAVG = "";
    String windGST = "";
    String weather = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickForConditions(View v) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://luca-teaching.appspot.com/weather/")
                .addConverterFactory(GsonConverterFactory.create())	//parse Gson string
                .client(httpClient)	//add logging
                .build();

        NicknameService service = retrofit.create(NicknameService.class);

        Call<Example> queryResponseCall =
                service.registerUser1(result, city, tempF, tempC, relHum, windAVG, windGST, weather);

        //Call retrofit asynchronously
        queryResponseCall.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Response<Example> response) {

                // If we get a server error
                if (response.code() == 500) {
                    TextView headerTV = (TextView) findViewById(R.id.headerText);
                    headerTV.setText("A server error occured, please press button again.");

                    // Clear all text
                    TextView cityTV = (TextView) findViewById(R.id.cityText);
                    TextView tempFTV = (TextView) findViewById(R.id.tempFText);
                    TextView tempCTV = (TextView) findViewById(R.id.tempCText);
                    TextView relHumTV = (TextView) findViewById(R.id.relHumText);
                    TextView windAVGTV = (TextView) findViewById(R.id.windAVGText);
                    TextView windGSTTV = (TextView) findViewById(R.id.windGSTText);
                    TextView weatherTV = (TextView) findViewById(R.id.weatherText);

                    cityTV.setText("");
                    tempFTV.setText("");
                    tempCTV.setText("");
                    relHumTV.setText("");
                    windAVGTV.setText("");
                    windGSTTV.setText("");
                    weatherTV.setText("");
                }

                // If we successfully connect to server
                if (response.code() == 200) {

                    if (response.body().response.result.equals("ok")) {
                        TextView headerTV = (TextView) findViewById(R.id.headerText);
                        headerTV.setText("");

                        city = "City: " + response.body().response.conditions.observationLocation.city;
                        tempF = "Temp (F): " + String.valueOf(response.body().response.conditions.tempF);
                        tempC = "Temp (C): " + String.valueOf(response.body().response.conditions.tempC);
                        relHum = "Relative Humidity: " + response.body().response.conditions.relativeHumidity;
                        windAVG = "Wind Avg: " + String.valueOf(response.body().response.conditions.windMph);
                        windGST = "Wind Gust: " + String.valueOf(response.body().response.conditions.windGustMph);
                        weather = "Weather: " + response.body().response.conditions.weather;

                        TextView cityTV = (TextView) findViewById(R.id.cityText);
                        TextView tempFTV = (TextView) findViewById(R.id.tempFText);
                        TextView tempCTV = (TextView) findViewById(R.id.tempCText);
                        TextView relHumTV = (TextView) findViewById(R.id.relHumText);
                        TextView windAVGTV = (TextView) findViewById(R.id.windAVGText);
                        TextView windGSTTV = (TextView) findViewById(R.id.windGSTText);
                        TextView weatherTV = (TextView) findViewById(R.id.weatherText);

                        cityTV.setText(city);
                        tempFTV.setText(tempF);
                        tempCTV.setText(tempC);
                        relHumTV.setText(relHum);
                        windAVGTV.setText(windAVG);
                        windGSTTV.setText(windGST);
                        weatherTV.setText(weather);

                    } else {
                        TextView headerTV = (TextView) findViewById(R.id.headerText);
                        headerTV.setText("An error occured, please press button again.");

                        // Clear all text
                        TextView cityTV = (TextView) findViewById(R.id.cityText);
                        TextView tempFTV = (TextView) findViewById(R.id.tempFText);
                        TextView tempCTV = (TextView) findViewById(R.id.tempCText);
                        TextView relHumTV = (TextView) findViewById(R.id.relHumText);
                        TextView windAVGTV = (TextView) findViewById(R.id.windAVGText);
                        TextView windGSTTV = (TextView) findViewById(R.id.windGSTText);
                        TextView weatherTV = (TextView) findViewById(R.id.weatherText);

                        cityTV.setText("");
                        tempFTV.setText("");
                        tempCTV.setText("");
                        relHumTV.setText("");
                        windAVGTV.setText("");
                        windGSTTV.setText("");
                        weatherTV.setText("");

                    }
                }
                //Log.i(LOG_TAG, "Code is: " + response.code());
                //Log.i(LOG_TAG, "The result is: " + response.body().response.result);
            }

            @Override
            public void onFailure(Throwable t) {
                // Log error here since request failed
            }
        });
    }

    /**
     * Foursquare api https://developer.foursquare.com/docs/venues/search
     */
    public interface NicknameService {
        @GET("default/get_weather/")
        Call<Example> registerUser1(@Query("result") String result,
                                    @Query("city") String city,
                                    @Query("temp_f") String tempF,
                                    @Query("temp_c") String tempC,
                                    @Query("relative_humidity") String relHum,
                                    @Query("wind_mph") String windAVG,
                                    @Query("wind_gust_mph") String windGST,
                                    @Query("weather") String weather);
    }
}
