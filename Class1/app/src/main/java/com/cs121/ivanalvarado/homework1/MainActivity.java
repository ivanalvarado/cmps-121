package com.cs121.ivanalvarado.homework1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickGreeting(View v) {
        // Let's get the new greeting.
        EditText et = (EditText) this.findViewById(R.id.editText);
        TextView tv = (TextView) findViewById(R.id.showText);
        String s = et.getText().toString();
        tv.setText(s);
        et.setText("");
    }
}
