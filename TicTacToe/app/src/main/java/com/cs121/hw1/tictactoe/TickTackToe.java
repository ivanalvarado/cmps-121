package com.cs121.hw1.tictactoe;

public class TickTackToe {
    private enum Player{
        Circle,
        Cross
    };

    public enum State{
        Unfilled,
        Circle,
        Cross
    }

    // Track which player is currently playing
    private Player player = Player.Circle;

    // Track state of every cell
    private State[] state = {State.Unfilled, State.Unfilled, State.Unfilled,
                             State.Unfilled, State.Unfilled, State.Unfilled,
                             State.Unfilled, State.Unfilled, State.Unfilled};

    TickTackToe() {

    }

    // Toggles the player so that they play alternatively
    private void Toggle(){
        switch (player){
            case Circle:
                player = Player.Cross;
                break;
            case Cross:
                player = Player.Circle;
                break;
        }
    }

    // Set the state of the given cell according to the player's turn
    public void setState(int x){
        if (state[x] == State.Unfilled){
            Toggle();
            switch (player){
                case Circle:
                    state[x] = State.Circle;
                    break;
                case Cross:
                    state[x] = State.Cross;
                    break;
            }
        }
    }

    // Give the state of given cell
    public State getState(int x){
        return state[x];
    }
}
