package com.cs121.hw1.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TickTackToe tickTackToe;
    int stateCount;
    int[] gameStates = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    boolean isTie = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tickTackToe = new TickTackToe();
    }

    public void clickBoard(View v){

        String Id = v.getResources().getResourceName(v.getId());//get the Id of Image Resource


        int position = Character.getNumericValue(Id.charAt(Id.length() - 1));//convert it to integer

        TextView tv = (TextView) findViewById(R.id.showText);
        //String pos = String.valueOf(position);
        tv.setVisibility(View.INVISIBLE);

        tickTackToe.setState(position);//Set the state of the current cell according to the player turn

        ImageButton imageButton = (ImageButton)v;
        switch (tickTackToe.getState(position))//also set the circle or cross in the view
        {
            case Circle:
                imageButton.setImageResource(R.drawable.circle);
                gameStates[position] = 1;
                break;
            case Cross:
                gameStates[position] = 2;
                imageButton.setImageResource(R.drawable.cross);
                break;
        }

        //----------------
        //
        // TicTacToe Grid
        // _0_|_1_|_2_
        // _3_|_4_|_5_
        //  6 | 7 | 8
        //
        //----------------

        // Check for winner ------------------------------------------------------

        // Winning Combo (0,1,2)
        if ((gameStates[0] != 0) && (gameStates[0] == gameStates[1]) && (gameStates[1] == gameStates[2])){
            isTie = false;
            if (gameStates[0] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button01 = (ImageButton) findViewById(R.id.imageButton01);
                button01.setBackgroundColor(0xFFFF0000);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[0] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button01 = (ImageButton) findViewById(R.id.imageButton01);
                button01.setBackgroundColor(0xFFFF0000);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (3,4,5)
        if ((gameStates[3] != 0) && (gameStates[3] == gameStates[4]) && (gameStates[4] == gameStates[5])){
            isTie = false;
            if (gameStates[3] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button10 = (ImageButton) findViewById(R.id.imageButton13);
                button10.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button12 = (ImageButton) findViewById(R.id.imageButton15);
                button12.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[3] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button10 = (ImageButton) findViewById(R.id.imageButton13);
                button10.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button12 = (ImageButton) findViewById(R.id.imageButton15);
                button12.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (6,7,8)
        if ((gameStates[6] != 0) && (gameStates[6] == gameStates[7]) && (gameStates[7] == gameStates[8])){
            isTie = false;
            if (gameStates[6] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                ImageButton button21 = (ImageButton) findViewById(R.id.imageButton27);
                button21.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[6] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                ImageButton button21 = (ImageButton) findViewById(R.id.imageButton27);
                button21.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (0,3,6)
        if ((gameStates[0] != 0) && (gameStates[0] == gameStates[3]) && (gameStates[3] == gameStates[6])){
            isTie = false;
            if (gameStates[0] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button10 = (ImageButton) findViewById(R.id.imageButton13);
                button10.setBackgroundColor(0xFFFF0000);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[0] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button10 = (ImageButton) findViewById(R.id.imageButton13);
                button10.setBackgroundColor(0xFFFF0000);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (1,4,7)
        if ((gameStates[1] != 0) && (gameStates[1] == gameStates[4]) && (gameStates[4] == gameStates[7])){
            isTie = false;
            if (gameStates[1] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button01 = (ImageButton) findViewById(R.id.imageButton01);
                button01.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button21 = (ImageButton) findViewById(R.id.imageButton27);
                button21.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[1] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button01 = (ImageButton) findViewById(R.id.imageButton01);
                button01.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button21 = (ImageButton) findViewById(R.id.imageButton27);
                button21.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (2,5,8)
        if ((gameStates[2] != 0) && (gameStates[2] == gameStates[5]) && (gameStates[5] == gameStates[8])){
            isTie = false;
            if (gameStates[2] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                ImageButton button12 = (ImageButton) findViewById(R.id.imageButton15);
                button12.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[2] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                ImageButton button12 = (ImageButton) findViewById(R.id.imageButton15);
                button12.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (0,4,8)
        if ((gameStates[0] != 0) && (gameStates[0] == gameStates[4]) && (gameStates[4] == gameStates[8])){
            isTie = false;
            if (gameStates[0] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[0] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button00 = (ImageButton) findViewById(R.id.imageButton00);
                button00.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button22 = (ImageButton) findViewById(R.id.imageButton28);
                button22.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Winning Combo (2,4,6)
        if ((gameStates[2] != 0) && (gameStates[2] == gameStates[4]) && (gameStates[4] == gameStates[6])){
            isTie = false;
            if (gameStates[2] == 2) {
                TextView xWins = (TextView) findViewById(R.id.xWon);
                xWins.setVisibility(View.VISIBLE);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
            if (gameStates[2] == 1) {
                TextView oWins = (TextView) findViewById(R.id.oWon);
                oWins.setVisibility(View.VISIBLE);
                ImageButton button02 = (ImageButton) findViewById(R.id.imageButton02);
                button02.setBackgroundColor(0xFFFF0000);
                ImageButton button11 = (ImageButton) findViewById(R.id.imageButton14);
                button11.setBackgroundColor(0xFFFF0000);
                ImageButton button20 = (ImageButton) findViewById(R.id.imageButton26);
                button20.setBackgroundColor(0xFFFF0000);
                disableVacantButtons();
            }
        }

        // Check for tie
        if (isTie) {
            stateCount = 0;
            for(int i = 0; i < gameStates.length; i++){
                if (gameStates[i] == 0){ break; }
                stateCount++;
                // If we've looped through entire state array w/o finding a '0'
                // then the game has resulted in a tie
                if (stateCount == 9){
                    TextView tieGame = (TextView) findViewById(R.id.tieG);
                    tieGame.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void disableVacantButtons() {
        if (gameStates[0] == 0) {
            ImageButton disableImgBtn00 = (ImageButton) findViewById(R.id.imageButton00);
            disableImgBtn00.setEnabled(false);
        }
        if (gameStates[1] == 0) {
            ImageButton disableImgBtn01 = (ImageButton) findViewById(R.id.imageButton01);
            disableImgBtn01.setEnabled(false);
        }
        if (gameStates[2] == 0) {
            ImageButton disableImgBtn02 = (ImageButton) findViewById(R.id.imageButton02);
            disableImgBtn02.setEnabled(false);
        }
        if (gameStates[3] == 0) {
            ImageButton disableImgBtn13 = (ImageButton) findViewById(R.id.imageButton13);
            disableImgBtn13.setEnabled(false);
        }
        if (gameStates[4] == 0) {
            ImageButton disableImgBtn14 = (ImageButton) findViewById(R.id.imageButton14);
            disableImgBtn14.setEnabled(false);
        }
        if (gameStates[5] == 0) {
            ImageButton disableImgBtn15 = (ImageButton) findViewById(R.id.imageButton15);
            disableImgBtn15.setEnabled(false);
        }
        if (gameStates[6] == 0) {
            ImageButton disableImgBtn26 = (ImageButton) findViewById(R.id.imageButton26);
            disableImgBtn26.setEnabled(false);
        }
        if (gameStates[7] == 0) {
            ImageButton disableImgBtn27 = (ImageButton) findViewById(R.id.imageButton27);
            disableImgBtn27.setEnabled(false);
        }
        if (gameStates[8] == 0) {
            ImageButton disableImgBtn28 = (ImageButton) findViewById(R.id.imageButton28);
            disableImgBtn28.setEnabled(false);
        }
    }

    // Start new game
    public void newGame(View v){ recreate(); }
}
